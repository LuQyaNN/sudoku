#pragma once
#include "Field.h"
class sudoku_maker
{
public:

	const Field template_field = array<int, 81>({
		1, 2, 3, 4, 5, 6, 7, 8, 9,
		4, 5, 6, 7, 8, 9, 1, 2, 3,
		7, 8, 9, 1, 2, 3, 4, 5, 6,
		2, 3, 4, 5, 6, 7, 8, 9, 1,
		5, 6, 7, 8, 9, 1, 2, 3, 4,
		8, 9, 1, 2, 3, 4, 5, 6, 7,
		3, 4, 5, 6, 7, 8, 9, 1,	2,
		6, 7, 8, 9, 1, 2, 3, 4,	5,
		9, 1, 2, 3, 4, 5, 6, 7, 8 });



	Field make(int interation_count, int num_empty);

	

	
};

