#pragma once
#include <array>
#include <vector>
#include "Var.h"
using namespace std;


class sudoku_solver
{
public:

	static void solve(class Field& a);
	static int first_empty(Field& a);
	static bool checkunic(std::array<int*, 9> & ar,bool with_zero);
	static void solve_support(Field& a, int index, vector<var>& posses);
	static bool already_val(std::array<int*, 9> & ar,int pos_check , int &pos_already_val);
};

