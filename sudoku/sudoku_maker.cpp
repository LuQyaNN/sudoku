#include "stdafx.h"
#include "sudoku_maker.h"
#include <random>
#include <ctime>
#include <cassert>

using namespace std;


Field sudoku_maker::make(int interation_count, int num_empty)
{

	srand(time(0));
	

	Field work_on_field(template_field);
	

	for (int i = 0; i < interation_count; i++)
	{
		
		
		int roll = rand()%23;

		switch (roll) {
		case 0: {work_on_field.swapsmallfieldcols(0, 1); break; }
		case 1: {work_on_field.swapsmallfieldcols(0, 2); break; }
		case 2: {work_on_field.swapsmallfieldcols(1, 2); break; }
		case 3: {work_on_field.swapsmallfieldrows(0, 1); break; }
		case 4: {work_on_field.swapsmallfieldrows(0, 2); break; }
		case 5: {work_on_field.swapsmallfieldrows(1, 2); break; }
		case 6: {work_on_field.swapcols(0, 1);			 break; }
		case 7: {work_on_field.swapcols(0, 2);			 break; }
		case 8: {work_on_field.swapcols(1, 2);			 break; }
		case 9: {work_on_field.swapcols(3, 5);			 break; }
		case 10: {work_on_field.swapcols(3, 4);			 break; }
		case 11: {work_on_field.swapcols(4, 5);			 break; }
		case 12: {work_on_field.swapcols(6, 8);			 break; }
		case 13: {work_on_field.swapcols(6, 7);			 break; }
		case 14: {work_on_field.swapcols(7, 8);			 break; }
		case 15: {work_on_field.swaprows(0, 1);			 break; }
		case 16: {work_on_field.swaprows(0, 2);			 break; }
		case 17: {work_on_field.swaprows(1, 2);			 break; }
		case 18: {work_on_field.swaprows(3, 5);			 break; }
		case 19: {work_on_field.swaprows(3, 4);			 break; }
		case 20: {work_on_field.swaprows(4, 5);			 break; }
		case 21: {work_on_field.swaprows(6, 8);			 break; }
		case 22: {work_on_field.swaprows(6, 7);			 break; }
		case 23: {work_on_field.swaprows(7, 8);			 break; }
		}
		
	}
	
	for (int i = 0; i < num_empty; ++i)
	{
		
		int roll = rand()%80;
		if (work_on_field.field[roll] == 0)
		{
			--i;
			continue;
		}
		work_on_field.field[roll] = 0;
	}
	return work_on_field;
}



