#include "stdafx.h"
#include "sudoku_solver.h"
#include "Field.h"
#include <algorithm>
#include <vector>
#include <ctime>
#include <thread>


using namespace std;



void sudoku_solver::solve(Field& a)
{
	
	vector<var> poss;
	for (int i = 0; i < 81; ++i)
	{
		

		if (a.field[i] == 0)
		{
			var tmp;
			tmp.pos = a.get_col_row_by_index(i);
			
			poss.push_back(tmp);
		}
	}
	for (auto& r : poss)
	{
		for (int i = 1; i < 10; ++i)
		{


			a.at(r.pos) = i;

			if (checkunic(a.getcol(r.pos.first), true)
				&& checkunic(a.getrow(r.pos.second), true)
				&& checkunic(a.get_small_field_by_at(r.pos), true))
			{
				r.values.push_back(i);

			}
			a.at(r.pos) = 0;
		}
	}
	
	while (!a.is_solved(poss))
	{
		
		

		//for (auto& p : poss)
		//{
		//	int tmpi = rand() % p.values.size();
		//	a.at(p.pos) = p.values[tmpi];
		//}
		
		solve_support(a, 0, poss);

		
		
	}
	
}

inline void sudoku_solver::solve_support(Field& a, int index, vector<var>& posses)
{
	
	if (index == posses.size())
		return;
	

	for (auto& v : posses[index].values)
	{
		if (checkunic(a.getcol(posses[index].pos.first), false))
			if (a.is_solved(posses))
				return;
		a.at(posses[index].pos) = v;
		
		if(a.is_solved(posses[index],true))
		{
		solve_support(a, index + 1, posses);
		}
		else
		{
			a.at(posses[index].pos) = 0;
		}
		
		
	}
	
	
	
}



int sudoku_solver::first_empty(Field& a)
{
	for (int i = 0; i < 81; ++i)
		if (a.field[i] == 0)return i;
}

bool sudoku_solver::already_val(std::array<int*, 9> & ar, int pos_check, int &pos_already_val)
{
	int value = *ar[pos_check];
	bool res = false;
	for (int i = 0; i < ar.size();++i)
	{
		if (*ar[i] == value)
		{
			res &= true;
			pos_already_val = i;
			break;
		}
	}
	return res;
}

bool sudoku_solver::checkunic(array<int*, 9>& ar, bool with_zero)
{
	
	
	unsigned int count = 0;
	bool res = false;
	for (int i = 1; i != 10; ++i)
	{
		for (auto& d : ar)
		{
			if (i == *d)
			{
				count++;
			}

		}
		if (with_zero)
		{
			if (count == 0)
			{
				res &= true;
				count = 0;
				continue;
			}
		}

		if (count == 1)
		{
			res &= true;
			count = 0;
			continue;
		}
		else
		{
			return false;
		}
	}
	return true;
	
	
}