#pragma once

#ifndef FIELD_H
#define FIELD_H

#include <array>
#include <iostream>
#include <cmath>
#include "sudoku_solver.h"
#include "Var.h"

#include <initializer_list>

using namespace std;

struct Field
{
	Field()
	{
		field.fill(0);
	}

	Field(array<int, 81>& f)
	{
		field.fill(0);
		field = f;
	}

	int& at(pair<int,int> p)
	{

		return field[9 * p.second + p.first];

	}

	int& at(int col, int row)
	{

		return field[9 * row + col];

	}

	bool is_solved(var value,bool with_zero)
	{
		bool solved = true;
		
		
			solved &= sudoku_solver::checkunic(getcol(value.pos.first), with_zero);
			solved &= sudoku_solver::checkunic(getrow(value.pos.second), with_zero);
			solved &= sudoku_solver::checkunic(get_small_field_by_at(value.pos), with_zero);
		
		return solved;
	}

	bool is_solved(vector<var> check_values)
	{
		bool solved = true;
		for (auto& value : check_values)
		{
			solved &= sudoku_solver::checkunic(getcol(value.pos.first), false);
			solved &= sudoku_solver::checkunic(getrow(value.pos.second), false);
			solved &= sudoku_solver::checkunic(get_small_field_by_at(value.pos), false);
		}
		return solved;
	}

	bool is_solved() noexcept
	{
		
		bool solved = true;
		for (int i = 0; i < 9; ++i)
		{
			solved&= sudoku_solver::checkunic(getcol(i),false);
			
		}
		for (int j = 0; j < 9; ++j)
		{
			solved &= sudoku_solver::checkunic(getrow(j), false);
		}
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				solved &= sudoku_solver::checkunic(getsmallfield(i, j), false);
			}
		}
		return solved;
	}

	pair<int, int> get_col_row_by_index(int index)
	{
		int col = 0, row = 0;
		row = floor(index / 9);
		if (index == 8 || index == 17 || index == 26 || index == 35 || index == 44 || index == 53 || index == 62 || index == 71 || index == 80)
		{
			col = 8;
		}
		else
		{
			col = (index + 1) % 9;
			if (col>0)
				col -= 1;
		}
			
		return pair<int, int>(col, row);
	}

	array<int*, 9> get_small_field_by_at(pair<int, int> p)
	{
		return get_small_field_by_at(p.first, p.second);
	}

	array<int*, 9> get_small_field_by_at(int col, int row)
	{
		if (col < 3)col = 0;
		if (col > 2 && col < 6)col = 1;
		if (col > 5)col = 2;

		if (row < 3)row = 0;
		if (row > 2 && row < 6)row = 1;
		if (row > 5)row = 2;

		return getsmallfield(col, row);
	}

	array<int*, 9> getcol(int col)
	{
		array<int*, 9> tmp;
		for (int y = 0; y < 9; ++y)
		{
			tmp[y] = &at(col, y);
		}
		return tmp;
	}

	array<int*, 9> getrow(int row)
	{
		array<int*, 9> tmp;
		for (int x = 0; x < 9; ++x)
		{
			tmp[x] = &at(x, row);
		}
		return tmp;
	}

	array<int*, 9> getsmallfield(int bigcol, int bigrow)
	{
		array<int*, 9> tmp = {};
		int arrpos = 0;
		for (int x = bigcol * 3; x <= bigcol * 3 + 2; ++x)
		{
			for (int y = bigrow * 3; y <= bigrow * 3 + 2; ++y)
			{
				tmp[arrpos] = &at(x, y);
				++arrpos;
			}
		}
		return tmp;
	}

	void swaprows(int r1, int r2)
	{
		array<int*, 9> a1 = getrow(r1);
		array<int*, 9> a2 = getrow(r2);
		for (int i = 0; i < 9; ++i)
		{
			swap(*a1[i], *a2[i]);

		}
	}

	void swapcols(int c1, int c2)
	{
		array<int*, 9> a1 = getcol(c1);
		array<int*, 9> a2 = getcol(c2);
		for (int i = 0; i < 9; ++i)
		{
			swap(*a1[i], *a2[i]);

		}
	}

	void swapsmallfieldcols(int c1, int c2)
	{
		for (int i = 0; i < 3; ++i)
		{
			array<int*, 9> a1 = getsmallfield(c1, i);
			array<int*, 9> a2 = getsmallfield(c2, i);
			for (int k = 0; k < 9; ++k)
			{
				swap(*a1[k], *a2[k]);

			}
		}
	}

	void swapsmallfieldrows(int r1, int r2)
	{
		for (int i = 0; i < 3; ++i)
		{
			array<int*, 9> a1 = getsmallfield( i,r1);
			array<int*, 9> a2 = getsmallfield(i, r2);
			for (int k = 0; k < 9; ++k)
			{
				swap(*a1[k], *a2[k]);

			}
		}
	}

	void printtostd()
	{
		for (int row = 0; row < 9; ++row)
		{
			for (int col = 0; col < 9; ++col)
			{
				printf("%i ", at(col, row));
			}
			printf("\n");
		}
	}




	array<int, 81> field;
};

#endif